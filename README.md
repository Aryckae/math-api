# Simple Math API Tutorial

## Overview
This project contains all associated files for my SwaggerHub tutorial on [My Blog](http://arne-ryckaerd.eu)

## Documentation

This folder contains all the documentation generated by SwaggerHub, a copy of the editor contents, and the Postman collection

## Latex

This folder contains a pdf version of the tutorial with the latex src files

## Code

This folder contains all of the code for the finished node server.

To run the server just execute `npm run start` from the directory and it will install all needed packages and will run the server on [localhost:8080](http://localhost:8080)

