class MemoryError extends Error {
  constructor(message) {
    super(message);
  }
}

module.exports = MemoryError;