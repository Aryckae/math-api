'use strict';

const MemoryError = require("./MemoryError");

class MemoryManager {
  recentResponses = [];

  getRecentResponses(skip = 0, limit = 10) {
    const entries = this.recentResponses.length;
    if (entries > 1) {
      if(skip < entries - 1 && limit > 0) {
        return {
          count: entries,
          results: this.recentResponses.slice(skip, skip + limit)
        }
      }
      throw new MemoryError('RangeOutOfBound');
    }
    else {
      throw new MemoryError('NoRecentResponsesAvailable');
    }
  }

  addResponse(result) {
    if(result.hasOwnProperty('type') && result.hasOwnProperty('result') && (result.hasOwnProperty('number') || result.hasOwnProperty('numbers'))) {
      console.log('Adding valid result');
      this.recentResponses.push(result);
    }
  }
}

module.exports = new MemoryManager();