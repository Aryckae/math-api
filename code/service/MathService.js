'use strict';

/**
 * Get the factorial of a number
 *
 * number basicNumber
 * returns basicNumber
 **/
exports.getFactorial = function (number) {
  if (number >= 1) {
    return number * this.getFactorial(number - 1);
  }
  else {
    return 1;
  }
}

/**
 * Get the sum of multiple numbers
 *
 * numbers List
 * returns basicNumber
 **/
exports.getSum = function (numbers) {
  return numbers.reduce((prev, next) => {
    return prev + next;
  });
}
