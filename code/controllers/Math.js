'use strict';

const MemoryError = require("../utils/MemoryError");

const utils = require('../utils/writer');
const Math = require('../service/MathService');
const memoryManager = require("../utils/memory");

// General
const processMath = (res, calculation) => {
  console.log('Received a Math request');
  try {
    const result = calculation();
    memoryManager.addResponse(result);
    utils.writeJson(res, result, 200);
  } catch (e) {
    console.log(e.message);
    res.status(500).send();
  }
}

// Factorial
const processFactorial = (res, number) => {
  processMath(res, () => {
    return {
      type: 'factorial',
      number,
      result: Math.getFactorial(number),
    }
  })
}

module.exports.getFactorial = function getFactorial(req, res, next, number) {
  if (number) {
    processFactorial(res, number);
  }
  else {
    res.status(400).send();
  }
};

module.exports.getFactorialJson = function getFactorialJson(req, res, next, body) {
  if (body.hasOwnProperty('number')) {
    processFactorial(res, body.number);
  } else {
    res.status(400).send();
  }
};

// Sum
const processSum = (res, numbers) => {
  processMath(res, () => {
    return {
      type: 'sum',
      numbers,
      result: Math.getSum(numbers),
    }
  });
}

module.exports.getSum = function getSum(req, res, next, numbers) {
  if (numbers) {
    processSum(res, numbers);
  } else {
    res.status(400).send();
  }
};

module.exports.getSumJson = function getSumJson(req, res, next, body) {
  if (body.hasOwnProperty('numbers')) {
    processSum(res, body.numbers);
  } else {
    res.status(400).send();
  }
};

// Recent
module.exports.getRecent = function getRecent(req, res, next, skip, limit) {
  try {
    const recent = memoryManager.getRecentResponses(skip, limit);
    utils.writeJson(res, recent, 200);
  }
  catch (e) {
    if(e instanceof MemoryError) {
      res.status(204).send();
    }
    else {
      console.error(e.message);
      res.status(500).send();
    }
  }
};